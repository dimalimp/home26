import { createStore } from 'redux'; // это просто функция которая поможет создать стор
import reducer from './rootReducer';

const store = createStore(reducer); // вот эта функцию. По факту она нужна что бы "познакомить" редюсер и стор (без этого знакоства у нас будет хранилище, но оно не будет понимат ькакая именно функция его пытается изменить)

export default store;
