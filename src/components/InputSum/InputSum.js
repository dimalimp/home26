import React, { PureComponent } from "react";
import { connect } from "react-redux";

class InputSum extends PureComponent {
    render() {
        const { sum } = this.props
        return (
            <input value={ sum } disabled></input>
        )
    }
}

const mapStateToProps = (state) => ({
    sum: state.sum,
})

export default connect(mapStateToProps)(InputSum)