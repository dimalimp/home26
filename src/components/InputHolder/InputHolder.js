import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { addSum } from '../../actionCreators';

class InputHolder extends PureComponent {
  state = {
    value: 0,
  };

  handleChange = (e) => {
    this.setState({ value: e.target.value });
  };

  handleClick = () => {
    const { value } = this.state;
    const { addSum } = this.props;

    addSum(value);
    this.setState({ value: '' });
  };

  render() {
    const { value } = this.state;
    return (
      <div className='wrapper'>
        <input value={ value } onChange={ this.handleChange } />
        <button onClick={ this.handleClick }>
          добавить
        </button>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  addSum: (num) => dispatch(addSum(num)),
});

export default connect(null, mapDispatchToProps)(InputHolder);
