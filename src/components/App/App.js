import InputHolder from '../InputHolder/InputHolder';
import InputSum from "../InputSum/InputSum";
import './App.scss';

function App() {
  return (
    <main>
      <InputHolder />
      <InputSum />
    </main>
  );
}

export default App;
